package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.*;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.article.api.domain.request.DocumentRequest;
import com.nbsaas.boot.article.api.domain.request.DocumentSearch;
import com.nbsaas.boot.article.api.domain.response.DocumentResponse;
import com.nbsaas.boot.article.api.domain.simple.DocumentSimple;
import com.nbsaas.boot.article.api.apis.DocumentApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/document")
public class DocumentController {


    @Resource
    private DocumentApi documentApi;

    @SearchData
    @RequiresPermissions("document")
    @RequestMapping("/search")
    public PageResponse <DocumentSimple> search(@RequestBody DocumentSearch request) {
        return documentApi.search(request);
    }

    @SearchData
    @RequiresPermissions("document")
    @RequestMapping("/list")
    public ListResponse<DocumentSimple> list(@RequestBody DocumentSearch request) {
        return documentApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @RequiresPermissions("document")
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <DocumentResponse> create(@RequestBody @Validated(AddOperator.class) DocumentRequest request) {
        return documentApi.create(request);
    }

   @RequiresPermissions("document")
   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<DocumentResponse> update(@RequestBody @Validated(UpdateOperator.class) DocumentRequest request) {
       return documentApi.update(request);
   }

    @SearchData
    @RequiresPermissions("document")
    @RequestMapping("/delete")
    public ResponseObject<?> delete(@RequestBody @Validated(DeleteOperator.class) DocumentRequest request) {
        return documentApi.delete(request);
    }

    @SearchData
    @RequiresPermissions("document")
    @RequestMapping("/view")
    public ResponseObject <DocumentResponse> view(@RequestBody @Validated(ViewOperator.class) DocumentRequest  request) {
        return documentApi.view(request);
    }
}