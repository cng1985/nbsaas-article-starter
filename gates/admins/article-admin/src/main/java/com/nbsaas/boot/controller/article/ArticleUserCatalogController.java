package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.*;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.article.api.domain.request.ArticleUserCatalogRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleUserCatalogSearch;
import com.nbsaas.boot.article.api.domain.response.ArticleUserCatalogResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleUserCatalogSimple;
import com.nbsaas.boot.article.api.apis.ArticleUserCatalogApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/articleUserCatalog")
public class ArticleUserCatalogController {


    @Resource
    private ArticleUserCatalogApi articleUserCatalogApi;

    @SearchData
    @RequiresPermissions("articleUserCatalog")
    @RequestMapping("/search")
    public PageResponse <ArticleUserCatalogSimple> search(@RequestBody ArticleUserCatalogSearch request) {
        return articleUserCatalogApi.search(request);
    }

    @SearchData
    @RequiresPermissions("articleUserCatalog")
    @RequestMapping("/list")
    public ListResponse<ArticleUserCatalogSimple> list(@RequestBody ArticleUserCatalogSearch request) {
        return articleUserCatalogApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @RequiresPermissions("articleUserCatalog")
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <ArticleUserCatalogResponse> create(@RequestBody @Validated(AddOperator.class) ArticleUserCatalogRequest request) {
        return articleUserCatalogApi.create(request);
    }

   @RequiresPermissions("articleUserCatalog")
   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<ArticleUserCatalogResponse> update(@RequestBody @Validated(UpdateOperator.class) ArticleUserCatalogRequest request) {
       return articleUserCatalogApi.update(request);
   }

    @SearchData
    @RequiresPermissions("articleUserCatalog")
    @RequestMapping("/delete")
    public ResponseObject<?> delete(@RequestBody @Validated(DeleteOperator.class) ArticleUserCatalogRequest request) {
        return articleUserCatalogApi.delete(request);
    }

    @SearchData
    @RequiresPermissions("articleUserCatalog")
    @RequestMapping("/view")
    public ResponseObject <ArticleUserCatalogResponse> view(@RequestBody @Validated(ViewOperator.class) ArticleUserCatalogRequest  request) {
        return articleUserCatalogApi.view(request);
    }
}