package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.*;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.article.api.domain.request.SensitiveWordRequest;
import com.nbsaas.boot.article.api.domain.request.SensitiveWordSearch;
import com.nbsaas.boot.article.api.domain.response.SensitiveWordResponse;
import com.nbsaas.boot.article.api.domain.simple.SensitiveWordSimple;
import com.nbsaas.boot.article.api.apis.SensitiveWordApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/sensitiveWord")
public class SensitiveWordController {


    @Resource
    private SensitiveWordApi sensitiveWordApi;

    @SearchData
    @RequiresPermissions("sensitiveWord")
    @RequestMapping("/search")
    public PageResponse <SensitiveWordSimple> search(@RequestBody SensitiveWordSearch request) {
        return sensitiveWordApi.search(request);
    }

    @SearchData
    @RequiresPermissions("sensitiveWord")
    @RequestMapping("/list")
    public ListResponse<SensitiveWordSimple> list(@RequestBody SensitiveWordSearch request) {
        return sensitiveWordApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @RequiresPermissions("sensitiveWord")
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <SensitiveWordResponse> create(@RequestBody @Validated(AddOperator.class) SensitiveWordRequest request) {
        return sensitiveWordApi.create(request);
    }

   @RequiresPermissions("sensitiveWord")
   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<SensitiveWordResponse> update(@RequestBody @Validated(UpdateOperator.class) SensitiveWordRequest request) {
       return sensitiveWordApi.update(request);
   }

    @SearchData
    @RequiresPermissions("sensitiveWord")
    @RequestMapping("/delete")
    public ResponseObject<?> delete(@RequestBody @Validated(DeleteOperator.class) SensitiveWordRequest request) {
        return sensitiveWordApi.delete(request);
    }

    @SearchData
    @RequiresPermissions("sensitiveWord")
    @RequestMapping("/view")
    public ResponseObject <SensitiveWordResponse> view(@RequestBody @Validated(ViewOperator.class) SensitiveWordRequest  request) {
        return sensitiveWordApi.view(request);
    }
}