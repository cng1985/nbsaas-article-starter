package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.*;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.article.api.domain.request.ArticleTagRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleTagSearch;
import com.nbsaas.boot.article.api.domain.response.ArticleTagResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleTagSimple;
import com.nbsaas.boot.article.api.apis.ArticleTagApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/articleTag")
public class ArticleTagController {


    @Resource
    private ArticleTagApi articleTagApi;

    @SearchData
    @RequiresPermissions("articleTag")
    @RequestMapping("/search")
    public PageResponse <ArticleTagSimple> search(@RequestBody ArticleTagSearch request) {
        return articleTagApi.search(request);
    }

    @SearchData
    @RequiresPermissions("articleTag")
    @RequestMapping("/list")
    public ListResponse<ArticleTagSimple> list(@RequestBody ArticleTagSearch request) {
        return articleTagApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @RequiresPermissions("articleTag")
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <ArticleTagResponse> create(@RequestBody @Validated(AddOperator.class) ArticleTagRequest request) {
        return articleTagApi.create(request);
    }

   @RequiresPermissions("articleTag")
   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<ArticleTagResponse> update(@RequestBody @Validated(UpdateOperator.class) ArticleTagRequest request) {
       return articleTagApi.update(request);
   }

    @SearchData
    @RequiresPermissions("articleTag")
    @RequestMapping("/delete")
    public ResponseObject<?> delete(@RequestBody @Validated(DeleteOperator.class) ArticleTagRequest request) {
        return articleTagApi.delete(request);
    }

    @SearchData
    @RequiresPermissions("articleTag")
    @RequestMapping("/view")
    public ResponseObject <ArticleTagResponse> view(@RequestBody @Validated(ViewOperator.class) ArticleTagRequest  request) {
        return articleTagApi.view(request);
    }
}