package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.*;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.article.api.domain.request.ArticleCatalogRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleCatalogSearch;
import com.nbsaas.boot.article.api.domain.response.ArticleCatalogResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleCatalogSimple;
import com.nbsaas.boot.article.api.apis.ArticleCatalogApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/articleCatalog")
public class ArticleCatalogController {


    @Resource
    private ArticleCatalogApi articleCatalogApi;

    @SearchData
    @RequiresPermissions("articleCatalog")
    @RequestMapping("/search")
    public PageResponse <ArticleCatalogSimple> search(@RequestBody ArticleCatalogSearch request) {
        return articleCatalogApi.search(request);
    }

    @SearchData
    @RequiresPermissions("articleCatalog")
    @RequestMapping("/list")
    public ListResponse<ArticleCatalogSimple> list(@RequestBody ArticleCatalogSearch request) {
        return articleCatalogApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @RequiresPermissions("articleCatalog")
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <ArticleCatalogResponse> create(@RequestBody @Validated(AddOperator.class) ArticleCatalogRequest request) {
        return articleCatalogApi.create(request);
    }

   @RequiresPermissions("articleCatalog")
   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<ArticleCatalogResponse> update(@RequestBody @Validated(UpdateOperator.class) ArticleCatalogRequest request) {
       return articleCatalogApi.update(request);
   }

    @SearchData
    @RequiresPermissions("articleCatalog")
    @RequestMapping("/delete")
    public ResponseObject<?> delete(@RequestBody @Validated(DeleteOperator.class) ArticleCatalogRequest request) {
        return articleCatalogApi.delete(request);
    }

    @SearchData
    @RequiresPermissions("articleCatalog")
    @RequestMapping("/view")
    public ResponseObject <ArticleCatalogResponse> view(@RequestBody @Validated(ViewOperator.class) ArticleCatalogRequest  request) {
        return articleCatalogApi.view(request);
    }
}