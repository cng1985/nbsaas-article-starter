package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.*;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.article.api.domain.request.SensitiveCategoryRequest;
import com.nbsaas.boot.article.api.domain.request.SensitiveCategorySearch;
import com.nbsaas.boot.article.api.domain.response.SensitiveCategoryResponse;
import com.nbsaas.boot.article.api.domain.simple.SensitiveCategorySimple;
import com.nbsaas.boot.article.api.apis.SensitiveCategoryApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/sensitiveCategory")
public class SensitiveCategoryController {


    @Resource
    private SensitiveCategoryApi sensitiveCategoryApi;

    @SearchData
    @RequiresPermissions("sensitiveCategory")
    @RequestMapping("/search")
    public PageResponse <SensitiveCategorySimple> search(@RequestBody SensitiveCategorySearch request) {
        return sensitiveCategoryApi.search(request);
    }

    @SearchData
    @RequiresPermissions("sensitiveCategory")
    @RequestMapping("/list")
    public ListResponse<SensitiveCategorySimple> list(@RequestBody SensitiveCategorySearch request) {
        return sensitiveCategoryApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @RequiresPermissions("sensitiveCategory")
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <SensitiveCategoryResponse> create(@RequestBody @Validated(AddOperator.class) SensitiveCategoryRequest request) {
        return sensitiveCategoryApi.create(request);
    }

   @RequiresPermissions("sensitiveCategory")
   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<SensitiveCategoryResponse> update(@RequestBody @Validated(UpdateOperator.class) SensitiveCategoryRequest request) {
       return sensitiveCategoryApi.update(request);
   }

    @SearchData
    @RequiresPermissions("sensitiveCategory")
    @RequestMapping("/delete")
    public ResponseObject<?> delete(@RequestBody @Validated(DeleteOperator.class) SensitiveCategoryRequest request) {
        return sensitiveCategoryApi.delete(request);
    }

    @SearchData
    @RequiresPermissions("sensitiveCategory")
    @RequestMapping("/view")
    public ResponseObject <SensitiveCategoryResponse> view(@RequestBody @Validated(ViewOperator.class) SensitiveCategoryRequest  request) {
        return sensitiveCategoryApi.view(request);
    }
}