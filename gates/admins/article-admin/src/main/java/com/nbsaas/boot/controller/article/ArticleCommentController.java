package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.*;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.article.api.domain.request.ArticleCommentRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleCommentSearch;
import com.nbsaas.boot.article.api.domain.response.ArticleCommentResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleCommentSimple;
import com.nbsaas.boot.article.api.apis.ArticleCommentApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/articleComment")
public class ArticleCommentController {


    @Resource
    private ArticleCommentApi articleCommentApi;

    @SearchData
    @RequiresPermissions("articleComment")
    @RequestMapping("/search")
    public PageResponse <ArticleCommentSimple> search(@RequestBody ArticleCommentSearch request) {
        return articleCommentApi.search(request);
    }

    @SearchData
    @RequiresPermissions("articleComment")
    @RequestMapping("/list")
    public ListResponse<ArticleCommentSimple> list(@RequestBody ArticleCommentSearch request) {
        return articleCommentApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @RequiresPermissions("articleComment")
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <ArticleCommentResponse> create(@RequestBody @Validated(AddOperator.class) ArticleCommentRequest request) {
        return articleCommentApi.create(request);
    }

   @RequiresPermissions("articleComment")
   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<ArticleCommentResponse> update(@RequestBody @Validated(UpdateOperator.class) ArticleCommentRequest request) {
       return articleCommentApi.update(request);
   }

    @SearchData
    @RequiresPermissions("articleComment")
    @RequestMapping("/delete")
    public ResponseObject<?> delete(@RequestBody @Validated(DeleteOperator.class) ArticleCommentRequest request) {
        return articleCommentApi.delete(request);
    }

    @SearchData
    @RequiresPermissions("articleComment")
    @RequestMapping("/view")
    public ResponseObject <ArticleCommentResponse> view(@RequestBody @Validated(ViewOperator.class) ArticleCommentRequest  request) {
        return articleCommentApi.view(request);
    }
}