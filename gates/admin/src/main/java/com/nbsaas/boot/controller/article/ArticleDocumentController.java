package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.*;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.article.api.domain.request.ArticleDocumentRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleDocumentSearch;
import com.nbsaas.boot.article.api.domain.response.ArticleDocumentResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleDocumentSimple;
import com.nbsaas.boot.article.api.apis.ArticleDocumentApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/articleDocument")
public class ArticleDocumentController {


    @Resource
    private ArticleDocumentApi articleDocumentApi;

    @SearchData
    @RequiresPermissions("articleDocument")
    @RequestMapping("/search")
    public PageResponse <ArticleDocumentSimple> search(@RequestBody ArticleDocumentSearch request) {
        return articleDocumentApi.search(request);
    }

    @SearchData
    @RequiresPermissions("articleDocument")
    @RequestMapping("/list")
    public ListResponse<ArticleDocumentSimple> list(@RequestBody ArticleDocumentSearch request) {
        return articleDocumentApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @RequiresPermissions("articleDocument")
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <ArticleDocumentResponse> create(@RequestBody @Validated(AddOperator.class) ArticleDocumentRequest request) {
        return articleDocumentApi.create(request);
    }

   @RequiresPermissions("articleDocument")
   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<ArticleDocumentResponse> update(@RequestBody @Validated(UpdateOperator.class) ArticleDocumentRequest request) {
       return articleDocumentApi.update(request);
   }

    @SearchData
    @RequiresPermissions("articleDocument")
    @RequestMapping("/delete")
    public ResponseObject<?> delete(@RequestBody @Validated(DeleteOperator.class) ArticleDocumentRequest request) {
        return articleDocumentApi.delete(request);
    }

    @SearchData
    @RequiresPermissions("articleDocument")
    @RequestMapping("/view")
    public ResponseObject <ArticleDocumentResponse> view(@RequestBody @Validated(ViewOperator.class) ArticleDocumentRequest  request) {
        return articleDocumentApi.view(request);
    }
}