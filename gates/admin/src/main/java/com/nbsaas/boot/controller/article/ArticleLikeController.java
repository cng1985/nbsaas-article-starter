package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.*;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.article.api.domain.request.ArticleLikeRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleLikeSearch;
import com.nbsaas.boot.article.api.domain.response.ArticleLikeResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleLikeSimple;
import com.nbsaas.boot.article.api.apis.ArticleLikeApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/articleLike")
public class ArticleLikeController {


    @Resource
    private ArticleLikeApi articleLikeApi;

    @SearchData
    @RequiresPermissions("articleLike")
    @RequestMapping("/search")
    public PageResponse <ArticleLikeSimple> search(@RequestBody ArticleLikeSearch request) {
        return articleLikeApi.search(request);
    }

    @SearchData
    @RequiresPermissions("articleLike")
    @RequestMapping("/list")
    public ListResponse<ArticleLikeSimple> list(@RequestBody ArticleLikeSearch request) {
        return articleLikeApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @RequiresPermissions("articleLike")
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <ArticleLikeResponse> create(@RequestBody @Validated(AddOperator.class) ArticleLikeRequest request) {
        return articleLikeApi.create(request);
    }

   @RequiresPermissions("articleLike")
   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<ArticleLikeResponse> update(@RequestBody @Validated(UpdateOperator.class) ArticleLikeRequest request) {
       return articleLikeApi.update(request);
   }

    @SearchData
    @RequiresPermissions("articleLike")
    @RequestMapping("/delete")
    public ResponseObject<?> delete(@RequestBody @Validated(DeleteOperator.class) ArticleLikeRequest request) {
        return articleLikeApi.delete(request);
    }

    @SearchData
    @RequiresPermissions("articleLike")
    @RequestMapping("/view")
    public ResponseObject <ArticleLikeResponse> view(@RequestBody @Validated(ViewOperator.class) ArticleLikeRequest  request) {
        return articleLikeApi.view(request);
    }
}