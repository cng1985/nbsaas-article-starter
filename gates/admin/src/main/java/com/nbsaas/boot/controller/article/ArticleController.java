package com.nbsaas.boot.controller.article;

import com.nbsaas.boot.rest.annotations.*;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.rest.response.PageResponse;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.article.api.domain.request.ArticleRequest;
import com.nbsaas.boot.article.api.domain.request.ArticleSearch;
import com.nbsaas.boot.article.api.domain.response.ArticleResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleSimple;
import com.nbsaas.boot.article.api.apis.ArticleApi;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;

/**
*  对外控制器
*/
@RequiresAuthentication
@RestController
@RequestMapping("/article")
public class ArticleController {


    @Resource
    private ArticleApi articleApi;

    @SearchData
    @RequiresPermissions("article")
    @RequestMapping("/search")
    public PageResponse <ArticleSimple> search(@RequestBody ArticleSearch request) {
        return articleApi.search(request);
    }

    @SearchData
    @RequiresPermissions("article")
    @RequestMapping("/list")
    public ListResponse<ArticleSimple> list(@RequestBody ArticleSearch request) {
        return articleApi.list(request);
    }

    /**
    * 添加数据
    *
    * @param request
    * @return
    */
    @RequiresPermissions("article")
    @CreateData
    @RequestMapping("/create")
    public ResponseObject <ArticleResponse> create(@RequestBody @Validated(AddOperator.class) ArticleRequest request) {
        return articleApi.create(request);
    }

   @RequiresPermissions("article")
   @UpdateData
   @RequestMapping("/update")
   public ResponseObject<ArticleResponse> update(@RequestBody @Validated(UpdateOperator.class) ArticleRequest request) {
       return articleApi.update(request);
   }

    @SearchData
    @RequiresPermissions("article")
    @RequestMapping("/delete")
    public ResponseObject<?> delete(@RequestBody @Validated(DeleteOperator.class) ArticleRequest request) {
        return articleApi.delete(request);
    }

    @SearchData
    @RequiresPermissions("article")
    @RequestMapping("/view")
    public ResponseObject <ArticleResponse> view(@RequestBody @Validated(ViewOperator.class) ArticleRequest  request) {
        return articleApi.view(request);
    }
}