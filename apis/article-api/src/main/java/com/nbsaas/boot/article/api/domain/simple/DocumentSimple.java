package com.nbsaas.boot.article.api.domain.simple;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;

/**
* 列表对象
*/
@Data
public class DocumentSimple implements Serializable {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;




            /**
            * 文档作者
            **/
                private Long creator;

            /**
            * 文档作者
            **/
                private String creatorName;

            /**
            * 
            **/
                private String logo;

            /**
            * 主键id
            **/
                private String id;

            /**
            * 
            **/
                private String title;

            /**
            * 添加时间
            **/
                private Date addDate;

            /**
            * 最新修改时间
            **/
                private Date lastDate;


}