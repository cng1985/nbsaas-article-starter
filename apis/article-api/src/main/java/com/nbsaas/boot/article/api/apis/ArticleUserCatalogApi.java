package com.nbsaas.boot.article.api.apis;

import com.nbsaas.boot.article.api.domain.request.ArticleUserCatalogRequest;
import com.nbsaas.boot.article.api.domain.simple.ArticleUserCatalogSimple;
import com.nbsaas.boot.article.api.domain.response.ArticleUserCatalogResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ArticleUserCatalogApi extends BaseApi<ArticleUserCatalogResponse, ArticleUserCatalogSimple, ArticleUserCatalogRequest> {


}
