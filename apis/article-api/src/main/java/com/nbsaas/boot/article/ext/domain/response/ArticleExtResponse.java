package com.nbsaas.boot.article.ext.domain.response;

import com.nbsaas.boot.article.api.domain.response.ArticleResponse;
import lombok.Data;

@Data
public class ArticleExtResponse extends ArticleResponse {

    private String note;
}
