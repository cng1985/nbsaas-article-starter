package com.nbsaas.boot.article.api.apis;

import com.nbsaas.boot.article.api.domain.request.DocumentRequest;
import com.nbsaas.boot.article.api.domain.simple.DocumentSimple;
import com.nbsaas.boot.article.api.domain.response.DocumentResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface DocumentApi extends BaseApi<DocumentResponse, DocumentSimple, DocumentRequest> {


}
