package com.nbsaas.boot.article.api.domain.field;


/**
*   字段映射类
*/
public class ArticleField  {



    /**
    * 
    **/
    public static final String  articleCatalog = "articleCatalog";


    /**
    * 
    **/
    public static final String  creator = "creator";


    /**
    * 
    **/
    public static final String  document = "document";


    /**
    * 
    **/
    public static final String  creatorName = "creatorName";


    /**
    * 
    **/
    public static final String  title = "title";


    /**
    * 
    **/
    public static final String  articleCatalogName = "articleCatalogName";


    /**
    * 添加时间
    **/
    public static final String  addDate = "addDate";


    /**
    * 
    **/
    public static final String  likeNum = "likeNum";


    /**
    * 
    **/
    public static final String  upNum = "upNum";


    /**
    * 
    **/
    public static final String  commentNum = "commentNum";


    /**
    * 
    **/
    public static final String  viewNum = "viewNum";


    /**
    * 
    **/
    public static final String  extData = "extData";


    /**
    * 
    **/
    public static final String  logo = "logo";


    /**
    * 主键id
    **/
    public static final String  id = "id";


    /**
    * 
    **/
    public static final String  introduction = "introduction";


    /**
    * 最新修改时间
    **/
    public static final String  lastDate = "lastDate";

}