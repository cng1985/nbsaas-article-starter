package com.nbsaas.boot.article.api.domain.request;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;
import com.nbsaas.boot.rest.request.RequestId;
/**
* 请求对象
*/
@Data
public class ArticleRequest implements Serializable,RequestId {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;



        /**
        * 
        **/
            private Long articleCatalog;

        /**
        * 
        **/
            private Long creator;

        /**
        * 
        **/
            private Long document;

        /**
        * 
        **/
            //private String creatorNameName;

        /**
        * 
        **/
            private String title;

        /**
        * 
        **/
            //private String articleCatalogNameName;

        /**
        * 添加时间
        **/
            private Date addDate;

        /**
        * 
        **/
            private Integer likeNum;

        /**
        * 
        **/
            private Integer upNum;

        /**
        * 
        **/
            private Integer commentNum;

        /**
        * 
        **/
            private Integer viewNum;

        /**
        * 
        **/
            private String extData;

        /**
        * 
        **/
            private String logo;

        /**
        * 主键id
        **/
            private Long id;

        /**
        * 
        **/
            private String introduction;

        /**
        * 最新修改时间
        **/
            private Date lastDate;
}