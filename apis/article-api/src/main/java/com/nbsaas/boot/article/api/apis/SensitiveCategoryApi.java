package com.nbsaas.boot.article.api.apis;

import com.nbsaas.boot.article.api.domain.request.SensitiveCategoryRequest;
import com.nbsaas.boot.article.api.domain.simple.SensitiveCategorySimple;
import com.nbsaas.boot.article.api.domain.response.SensitiveCategoryResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface SensitiveCategoryApi extends BaseApi<SensitiveCategoryResponse, SensitiveCategorySimple, SensitiveCategoryRequest> {


}
