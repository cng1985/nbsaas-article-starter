package com.nbsaas.boot.article.api.domain.simple;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;

/**
* 列表对象
*/
@Data
public class ArticleCommentSimple implements Serializable {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;




            /**
            * 
            **/
                private String note;

            /**
            * 
            **/
                private Long creator;

            /**
            * 主键id
            **/
                private Long id;

            /**
            * 
            **/
                private String title;

            /**
            * 添加时间
            **/
                private Date addDate;

            /**
            * 
            **/
                private Long article;

            /**
            * 最新修改时间
            **/
                private Date lastDate;


}