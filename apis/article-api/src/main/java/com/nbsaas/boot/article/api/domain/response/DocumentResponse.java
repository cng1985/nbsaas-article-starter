package com.nbsaas.boot.article.api.domain.response;

import lombok.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
* 响应对象
*/
@Getter
@Setter
@ToString(callSuper = true)
public class DocumentResponse  implements Serializable {
/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;


        /**
        * 文档内容
        **/
            private String note;

        /**
        * 文档作者
        **/
            private Long creator;

        /**
        * 文档作者
        **/
            private String creatorName;

        /**
        * 
        **/
            private String logo;

        /**
        * 主键id
        **/
            private String id;

        /**
        * 
        **/
            private String title;

        /**
        * 添加时间
        **/
            private Date addDate;

        /**
        * 最新修改时间
        **/
            private Date lastDate;

}