package com.nbsaas.boot.article.api.domain.request;

import com.nbsaas.boot.rest.filter.Operator;
import com.nbsaas.boot.rest.filter.Search;
import com.nbsaas.boot.rest.request.PageRequest;
import lombok.*;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
* 搜索bean
*/
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ArticleSearch   extends PageRequest implements Serializable {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;


    @Search(name = "title",operator = Operator.like)
    private String title;

    @Search(name = "articleCatalog.id",operator = Operator.eq)
    private Long articleCatalog;


            /**
            * 
            **/
            @Search(name = "likeNum",operator = Operator.eq)
            private Integer likeNum;

            /**
            * 
            **/
            @Search(name = "upNum",operator = Operator.eq)
            private Integer upNum;

            /**
            * 
            **/
            @Search(name = "commentNum",operator = Operator.eq)
            private Integer commentNum;

            /**
            * 
            **/
            @Search(name = "viewNum",operator = Operator.eq)
            private Integer viewNum;

            /**
            * 
            **/
            @Search(name = "extData",operator = Operator.like)
            private String extData;

            /**
            * 
            **/
            @Search(name = "logo",operator = Operator.like)
            private String logo;

            /**
            * 主键id
            **/
            @Search(name = "id",operator = Operator.eq)
            private Long id;

            /**
            * 
            **/
            @Search(name = "introduction",operator = Operator.like)
            private String introduction;



}