package com.nbsaas.boot.article.api.domain.field;


/**
*   字段映射类
*/
public class ArticleCommentField  {



    /**
    * 
    **/
    public static final String  note = "note";


    /**
    * 
    **/
    public static final String  creator = "creator";


    /**
    * 主键id
    **/
    public static final String  id = "id";


    /**
    * 
    **/
    public static final String  title = "title";


    /**
    * 添加时间
    **/
    public static final String  addDate = "addDate";


    /**
    * 
    **/
    public static final String  article = "article";


    /**
    * 最新修改时间
    **/
    public static final String  lastDate = "lastDate";

}