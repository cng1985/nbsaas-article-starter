package com.nbsaas.boot.article.api.domain.field;


/**
*   字段映射类
*/
public class DocumentField  {



    /**
    * 文档内容
    **/
    public static final String  note = "note";


    /**
    * 文档作者
    **/
    public static final String  creator = "creator";


    /**
    * 文档作者
    **/
    public static final String  creatorName = "creatorName";


    /**
    * 
    **/
    public static final String  logo = "logo";


    /**
    * 主键id
    **/
    public static final String  id = "id";


    /**
    * 
    **/
    public static final String  title = "title";


    /**
    * 添加时间
    **/
    public static final String  addDate = "addDate";


    /**
    * 最新修改时间
    **/
    public static final String  lastDate = "lastDate";

}