package com.nbsaas.boot.article.api.apis;

import com.nbsaas.boot.article.api.domain.request.SensitiveWordRequest;
import com.nbsaas.boot.article.api.domain.simple.SensitiveWordSimple;
import com.nbsaas.boot.article.api.domain.response.SensitiveWordResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface SensitiveWordApi extends BaseApi<SensitiveWordResponse, SensitiveWordSimple, SensitiveWordRequest> {


}
