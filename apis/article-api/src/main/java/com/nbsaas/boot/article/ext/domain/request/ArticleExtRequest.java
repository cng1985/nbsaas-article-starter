package com.nbsaas.boot.article.ext.domain.request;

import com.nbsaas.boot.article.api.domain.request.ArticleRequest;
import lombok.Data;

@Data
public class ArticleExtRequest extends ArticleRequest {

    private String note;

}
