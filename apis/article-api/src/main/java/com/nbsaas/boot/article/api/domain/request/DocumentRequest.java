package com.nbsaas.boot.article.api.domain.request;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;
import com.nbsaas.boot.rest.request.RequestId;
/**
* 请求对象
*/
@Data
public class DocumentRequest implements Serializable,RequestId {

/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;



        /**
        * 文档内容
        **/
            private String note;

        /**
        * 文档作者
        **/
            private Long creator;

        /**
        * 文档作者
        **/
            //private String creatorNameName;

        /**
        * 
        **/
            private String logo;

        /**
        * 主键id
        **/
            private String id;

        /**
        * 
        **/
            private String title;

        /**
        * 添加时间
        **/
            private Date addDate;

        /**
        * 最新修改时间
        **/
            private Date lastDate;
}