package com.nbsaas.boot.article.api.apis;

import com.nbsaas.boot.article.api.domain.request.ArticleTagRequest;
import com.nbsaas.boot.article.api.domain.simple.ArticleTagSimple;
import com.nbsaas.boot.article.api.domain.response.ArticleTagResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ArticleTagApi extends BaseApi<ArticleTagResponse, ArticleTagSimple, ArticleTagRequest> {


}
