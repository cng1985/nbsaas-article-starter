package com.nbsaas.boot.article.api.apis;

import com.nbsaas.boot.article.api.domain.request.ArticleCommentRequest;
import com.nbsaas.boot.article.api.domain.simple.ArticleCommentSimple;
import com.nbsaas.boot.article.api.domain.response.ArticleCommentResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ArticleCommentApi extends BaseApi<ArticleCommentResponse, ArticleCommentSimple, ArticleCommentRequest> {


}
