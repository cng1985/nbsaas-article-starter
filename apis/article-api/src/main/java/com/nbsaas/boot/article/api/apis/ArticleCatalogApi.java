package com.nbsaas.boot.article.api.apis;

import com.nbsaas.boot.article.api.domain.request.ArticleCatalogRequest;
import com.nbsaas.boot.article.api.domain.simple.ArticleCatalogSimple;
import com.nbsaas.boot.article.api.domain.response.ArticleCatalogResponse;
import com.nbsaas.boot.rest.api.BaseApi;


/**
* 对外接口
*/
public interface ArticleCatalogApi extends BaseApi<ArticleCatalogResponse, ArticleCatalogSimple, ArticleCatalogRequest> {


}
