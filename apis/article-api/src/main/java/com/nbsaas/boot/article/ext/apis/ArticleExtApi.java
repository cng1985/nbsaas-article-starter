package com.nbsaas.boot.article.ext.apis;

import com.nbsaas.boot.article.ext.domain.request.ArticleExtRequest;
import com.nbsaas.boot.article.ext.domain.response.ArticleExtResponse;
import com.nbsaas.boot.rest.response.ResponseObject;

public interface ArticleExtApi {

    /**
     * 添加文章
     *
     * @param request
     * @return
     */
    ResponseObject<ArticleExtResponse> create(ArticleExtRequest request);


    /**
     * 更新文章
     *
     * @param request
     * @return
     */
    ResponseObject<ArticleExtResponse> update(ArticleExtRequest request);
}
