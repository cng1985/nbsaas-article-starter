package com.nbsaas.boot.article.api.domain.response;

import lombok.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
* 响应对象
*/
@Getter
@Setter
@ToString(callSuper = true)
public class ArticleResponse  implements Serializable {
/**
* 序列化参数
*/
private static final long serialVersionUID = 1L;


        /**
        * 
        **/
            private Long articleCatalog;

        /**
        * 
        **/
            private String note;

        /**
        * 
        **/
            private Long creator;

        /**
        * 
        **/
            private Long document;

        /**
        * 
        **/
            private String creatorName;

        /**
        * 
        **/
            private String title;

        /**
        * 
        **/
            private String articleCatalogName;

        /**
        * 添加时间
        **/
            private Date addDate;

        /**
        * 
        **/
            private Integer likeNum;

        /**
        * 
        **/
            private Integer upNum;

        /**
        * 
        **/
            private Integer commentNum;

        /**
        * 
        **/
            private Integer viewNum;

        /**
        * 
        **/
            private String extData;

        /**
        * 
        **/
            private String logo;

        /**
        * 主键id
        **/
            private Long id;

        /**
        * 
        **/
            private String introduction;

        /**
        * 最新修改时间
        **/
            private Date lastDate;

}