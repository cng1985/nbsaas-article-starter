package com.nbsaas.boot.article.data.entity;

import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import com.nbsaas.boot.jpa.data.entity.LongEntity;
import com.nbsaas.boot.jpa.data.entity.User;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by cng19 on 2017/12/2.
 */


@Data
@Entity
@Table(name = "bs_cms_article_like")
public class ArticleLike extends AbstractEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Article article;

    @ManyToOne(fetch = FetchType.LAZY)
    private User creator;

}
