package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleUserCatalog;
import com.nbsaas.boot.article.api.domain.response.ArticleUserCatalogResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;
/**
* 实体对象转化成响应对象
*/

public class ArticleUserCatalogResponseConvert  implements Converter<ArticleUserCatalogResponse,ArticleUserCatalog> {

    @Override
    public ArticleUserCatalogResponse convert(ArticleUserCatalog source) {
        ArticleUserCatalogResponse  result = new  ArticleUserCatalogResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

