package com.nbsaas.boot.article.data.repository;

import com.nbsaas.boot.article.data.entity.ArticleCatalog;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface ArticleCatalogRepository  extends  JpaRepositoryImplementation<ArticleCatalog, Serializable>{

}