package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.Document;
import com.nbsaas.boot.article.api.domain.response.DocumentResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;
/**
* 实体对象转化成响应对象
*/

public class DocumentResponseConvert  implements Converter<DocumentResponse,Document> {

    @Override
    public DocumentResponse convert(Document source) {
        DocumentResponse  result = new  DocumentResponse();
        BeanDataUtils.copyProperties(source, result);
                    if(source.getCreator()!=null){
                        result.setCreator(source.getCreator().getId());
                    }
                    if(source.getCreator()!=null){
                        result.setCreatorName(source.getCreator().getName());
                    }
        return result;
    }

}

