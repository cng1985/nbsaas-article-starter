package com.nbsaas.boot.article.data.repository;

import com.nbsaas.boot.article.data.entity.ArticleTag;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface ArticleTagRepository  extends  JpaRepositoryImplementation<ArticleTag, Serializable>{

}