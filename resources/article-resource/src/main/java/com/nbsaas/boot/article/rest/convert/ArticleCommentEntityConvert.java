package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleComment;
import com.nbsaas.boot.article.api.domain.request.ArticleCommentRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;
            import com.nbsaas.boot.jpa.data.entity.User;
            import com.nbsaas.boot.article.data.entity.Article;

/**
* 请求对象转换成实体对象
*/

public class ArticleCommentEntityConvert  implements Converter<ArticleComment, ArticleCommentRequest> {

    @Override
    public ArticleComment convert(ArticleCommentRequest source) {
        ArticleComment result = new ArticleComment();
        BeanDataUtils.copyProperties(source, result);
                    if(source.getCreator()!=null){
                    User creator =new User();
                    creator.setId(source.getCreator());
                    result.setCreator(creator);
                    }
                    if(source.getArticle()!=null){
                    Article article =new Article();
                    article.setId(source.getArticle());
                    result.setArticle(article);
                    }
        return result;
    }
}

