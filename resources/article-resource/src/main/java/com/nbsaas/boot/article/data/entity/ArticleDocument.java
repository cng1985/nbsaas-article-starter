package com.nbsaas.boot.article.data.entity;


import com.nbsaas.boot.code.annotation.FormAnnotation;
import com.nbsaas.boot.jpa.data.entity.LongEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@FormAnnotation(title = "文章管理", model = "文章", menu = "1,101,102")
@Data
@Entity
@Table(name = "bs_cms_article_document")
public class ArticleDocument extends LongEntity {


    @Column(length = 5000)
    private String note;

}
