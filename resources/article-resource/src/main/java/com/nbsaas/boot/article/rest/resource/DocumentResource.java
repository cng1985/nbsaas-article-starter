package com.nbsaas.boot.article.rest.resource;

import com.nbsaas.boot.article.api.apis.DocumentApi;
import com.nbsaas.boot.article.data.entity.Document;
import com.nbsaas.boot.article.api.domain.request.DocumentRequest;
import com.nbsaas.boot.article.api.domain.response.DocumentResponse;
import com.nbsaas.boot.article.api.domain.simple.DocumentSimple;
import com.nbsaas.boot.article.rest.convert.DocumentSimpleConvert;
import com.nbsaas.boot.article.rest.convert.DocumentEntityConvert;
import com.nbsaas.boot.article.rest.convert.DocumentResponseConvert;
import com.nbsaas.boot.article.data.repository.DocumentRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.function.Function;
/**
*   业务接口实现
*/
@Transactional
@Service
public class DocumentResource extends BaseResource<Document,DocumentResponse, DocumentSimple, DocumentRequest>  implements DocumentApi {

    @Resource
    private DocumentRepository documentRepository;

    @Override
    public JpaRepositoryImplementation<Document, Serializable> getJpaRepository() {
        return documentRepository;
    }

    @Override
    public Function<Document, DocumentSimple> getConvertSimple() {
        return new DocumentSimpleConvert();
    }

    @Override
    public Function<DocumentRequest, Document> getConvertForm() {
        return new DocumentEntityConvert();
    }

    @Override
    public Function<Document, DocumentResponse> getConvertResponse() {
        return new DocumentResponseConvert();
    }




}


