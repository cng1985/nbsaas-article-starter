package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleCatalog;
import com.nbsaas.boot.article.api.domain.simple.ArticleCatalogSimple;

import com.nbsaas.boot.rest.api.Converter;
    import java.util.stream.Collectors;
    import lombok.Data;

/**
* 列表对象转换器
*/

    @Data
public class ArticleCatalogSimpleConvert implements Converter<ArticleCatalogSimple, ArticleCatalog> {


    private int fetch;


@Override
public ArticleCatalogSimple convert(ArticleCatalog source) {
    ArticleCatalogSimple result = new ArticleCatalogSimple();

                result.setAmount(source.getAmount());
                result.setCode(source.getCode());
                result.setDepth(source.getDepth());
                result.setName(source.getName());
                result.setIds(source.getIds());
                result.setSortNum(source.getSortNum());
                result.setId(source.getId());
                result.setLft(source.getLft());
                result.setAddDate(source.getAddDate());
                result.setRgt(source.getRgt());
                result.setLastDate(source.getLastDate());

        result.setLabel(source.getName());
        result.setValue(""+source.getId());
        if (source.getChildren()!=null&&source.getChildren().size()>0){
            if (fetch!=0){
                result.setChildren(source.getChildren().stream().map(this).collect(Collectors.toList()));
            }
        }else{
        }

    return result;
}

}