package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleLike;
import com.nbsaas.boot.article.api.domain.request.ArticleLikeRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class ArticleLikeEntityConvert  implements Converter<ArticleLike, ArticleLikeRequest> {

    @Override
    public ArticleLike convert(ArticleLikeRequest source) {
        ArticleLike result = new ArticleLike();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

