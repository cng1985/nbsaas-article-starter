package com.nbsaas.boot.article.ext.resource;

import com.nbsaas.boot.article.api.apis.ArticleApi;
import com.nbsaas.boot.article.api.apis.ArticleDocumentApi;
import com.nbsaas.boot.article.api.domain.request.ArticleDocumentRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleDocumentResponse;
import com.nbsaas.boot.article.api.domain.response.ArticleResponse;
import com.nbsaas.boot.article.data.entity.Article;
import com.nbsaas.boot.article.data.entity.ArticleDocument;
import com.nbsaas.boot.article.data.repository.ArticleDocumentRepository;
import com.nbsaas.boot.article.data.repository.ArticleRepository;
import com.nbsaas.boot.article.ext.apis.ArticleExtApi;
import com.nbsaas.boot.article.ext.domain.request.ArticleExtRequest;
import com.nbsaas.boot.article.ext.domain.response.ArticleExtResponse;
import com.nbsaas.boot.article.rest.convert.ArticleEntityConvert;
import com.nbsaas.boot.rest.response.ResponseObject;
import com.nbsaas.boot.utils.BeanDataUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class ArticleExtResource implements ArticleExtApi {

    @Resource
    private ArticleApi articleApi;

    @Resource
    private ArticleRepository articleRepository;

    @Resource
    private ArticleDocumentRepository articleDocumentRepository;

    @Resource
    private ArticleDocumentApi articleDocumentApi;

    @Transactional
    @Override
    public ResponseObject<ArticleExtResponse> create(ArticleExtRequest request) {
        ResponseObject<ArticleExtResponse> result = new ResponseObject<>();

        ArticleDocumentRequest document=new ArticleDocumentRequest();
        document.setNote(request.getNote());
        ArticleDocumentResponse documentRes = articleDocumentApi.createData(document);
        request.setDocument(documentRes.getId());
        ResponseObject<ArticleResponse> res = articleApi.create(request);
        if (res.getCode()!=200){
            result.setCode(res.getCode());
            result.setMsg(res.getMsg());
            return result;
        }
        ArticleExtResponse response=new ArticleExtResponse();
        BeanUtils.copyProperties(res.getData(),response);
        response.setNote(documentRes.getNote());

        result.setData(response);
        return result;

    }

    @Transactional
    @Override
    public ResponseObject<ArticleExtResponse> update(ArticleExtRequest request) {
        ResponseObject<ArticleExtResponse> result = new ResponseObject<>();
        Optional<Article> article = articleRepository.findById(request.getId());
        if (!article.isPresent()) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        Article bean = article.get();
        ArticleEntityConvert entityConvert=new ArticleEntityConvert();
        Article entity = entityConvert.convert(request);
        BeanDataUtils.copyProperties(entity, bean);
        ArticleDocument document = bean.getDocument();
        if (document==null){
            document=new ArticleDocument();
            document.setNote(request.getNote());
            articleDocumentRepository.save(document);
            bean.setDocument(document);
        }else{
            Optional<ArticleDocument> temp = articleDocumentRepository.findById(document.getId());
            temp.ifPresent(shopArticleDocument -> shopArticleDocument.setNote(request.getNote()));
        }

        return result;
    }

}
