package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.Article;
import com.nbsaas.boot.article.api.domain.simple.ArticleSimple;

import com.nbsaas.boot.rest.api.Converter;

/**
* 列表对象转换器
*/

public class ArticleSimpleConvert implements Converter<ArticleSimple, Article> {




@Override
public ArticleSimple convert(Article source) {
    ArticleSimple result = new ArticleSimple();

                if(source.getArticleCatalog()!=null){
                    result.setArticleCatalog(source.getArticleCatalog().getId());
                }
                if(source.getCreator()!=null){
                    result.setCreator(source.getCreator().getId());
                }
                if(source.getDocument()!=null){
                    result.setDocument(source.getDocument().getId());
                }
                if(source.getCreator()!=null){
                    result.setCreatorName(source.getCreator().getName());
                }
                result.setTitle(source.getTitle());
                if(source.getArticleCatalog()!=null){
                    result.setArticleCatalogName(source.getArticleCatalog().getName());
                }
                result.setAddDate(source.getAddDate());
                result.setLikeNum(source.getLikeNum());
                result.setUpNum(source.getUpNum());
                result.setCommentNum(source.getCommentNum());
                result.setViewNum(source.getViewNum());
                result.setExtData(source.getExtData());
                result.setLogo(source.getLogo());
                result.setId(source.getId());
                result.setIntroduction(source.getIntroduction());
                result.setLastDate(source.getLastDate());


    return result;
}

}