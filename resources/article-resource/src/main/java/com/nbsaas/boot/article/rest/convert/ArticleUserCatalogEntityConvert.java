package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleUserCatalog;
import com.nbsaas.boot.article.api.domain.request.ArticleUserCatalogRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class ArticleUserCatalogEntityConvert  implements Converter<ArticleUserCatalog, ArticleUserCatalogRequest> {

    @Override
    public ArticleUserCatalog convert(ArticleUserCatalogRequest source) {
        ArticleUserCatalog result = new ArticleUserCatalog();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

