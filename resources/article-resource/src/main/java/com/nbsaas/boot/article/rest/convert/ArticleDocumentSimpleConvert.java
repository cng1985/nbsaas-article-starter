package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleDocument;
import com.nbsaas.boot.article.api.domain.simple.ArticleDocumentSimple;

import com.nbsaas.boot.rest.api.Converter;

/**
* 列表对象转换器
*/

public class ArticleDocumentSimpleConvert implements Converter<ArticleDocumentSimple, ArticleDocument> {




@Override
public ArticleDocumentSimple convert(ArticleDocument source) {
    ArticleDocumentSimple result = new ArticleDocumentSimple();

                result.setNote(source.getNote());
                result.setId(source.getId());


    return result;
}

}