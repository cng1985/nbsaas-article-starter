package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleTag;
import com.nbsaas.boot.article.api.domain.simple.ArticleTagSimple;

import com.nbsaas.boot.rest.api.Converter;

/**
* 列表对象转换器
*/

public class ArticleTagSimpleConvert implements Converter<ArticleTagSimple, ArticleTag> {




@Override
public ArticleTagSimple convert(ArticleTag source) {
    ArticleTagSimple result = new ArticleTagSimple();

                result.setSize(source.getSize());
                result.setName(source.getName());
                result.setId(source.getId());
                result.setAddDate(source.getAddDate());
                result.setLastDate(source.getLastDate());


    return result;
}

}