package com.nbsaas.boot.article.data.entity;

import com.nbsaas.boot.code.annotation.*;
import com.nbsaas.boot.jpa.data.entity.LongEntity;
import com.nbsaas.boot.jpa.data.entity.User;
import com.nbsaas.boot.rest.filter.Operator;
import lombok.Data;
import org.hibernate.annotations.Comment;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * 文章
 *
 * @author 年高
 */
@BeanExt(items = {
        @FormExtField(fieldName = "note", parentField = "note", parent = "document", fieldClass = String.class, simple = false)
})
@ComposeView
@CreateByUser
@FormAnnotation(title = "文章管理", model = "文章", menu = "1,101,102")
@Data
@Entity
@Table(name = "bs_cms_article")
public class Article extends LongEntity {

    public static Article fromId(Long id){
        Article result=new Article();
        result.setId(id);
        return result;
    }

    /**
     * 文章标题
     */
    @SearchItem(label = "标题",name = "title")
    @FormField(title = "标题", grid = true, col = 22,width = "200",required = true)
    private String title;


    /**
     * 封面
     */
    @FormField(title = "封面", grid = true, col = 22,required = true,type = InputType.image)
    private String logo;
    
    
    /**
     * 文章分类
     */
    @SearchItem(label = "文章分类",name = "articleCatalog",key = "articleCatalog.id",operator = Operator.eq,classType =Long.class,show = false)
    @FieldConvert
    @FieldName
    @FormField(title = "文章分类", width = "200",grid = true, col =22,type = InputType.select,option = "articleCatalog")
    @ManyToOne(fetch = FetchType.LAZY)
    private ArticleCatalog articleCatalog;

    /**
     *     @FormField(title = "文章内容", grid = false, col = 22,type = InputType.richText)
     *     private String note;
     */

    /**
     * 文章评论数量
     */
    private Integer commentNum;

    /**
     * 喜欢的数量
     */
    private Integer likeNum;


    @ManyToOne(fetch = FetchType.LAZY)
    private ArticleDocument articleDocument;

    /**
     * 文章扩展信息
     */
    private String extData;



    /**
     * 文章介绍
     */
    private String introduction;
    @JoinTable(name = "article_link_tag")
    @ManyToMany
    private Set<ArticleTag> tags;



    /**
     * 点赞数量
     */
    private Integer upNum;

    /**
     * 文章作者
     */
    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private User creator;
    /**
     * 文章查看数量
     */
    private Integer viewNum;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private ArticleDocument document;


    /**
     * 添加时间
     */
    @FormField(title = "添加时间", grid = true, width = "11111", ignore = true)
    @Comment("添加时间")
    private Date addDate;

    /**
     * 最新修改时间
     */
    @Comment("最新修改时间")
    private Date lastDate;
}
