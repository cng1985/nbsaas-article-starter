package com.nbsaas.boot.article.rest.resource;

import com.nbsaas.boot.article.api.apis.ArticleCommentApi;
import com.nbsaas.boot.article.data.entity.ArticleComment;
import com.nbsaas.boot.article.api.domain.request.ArticleCommentRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleCommentResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleCommentSimple;
import com.nbsaas.boot.article.rest.convert.ArticleCommentSimpleConvert;
import com.nbsaas.boot.article.rest.convert.ArticleCommentEntityConvert;
import com.nbsaas.boot.article.rest.convert.ArticleCommentResponseConvert;
import com.nbsaas.boot.article.data.repository.ArticleCommentRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.function.Function;
/**
*   业务接口实现
*/
@Transactional
@Service
public class ArticleCommentResource extends BaseResource<ArticleComment,ArticleCommentResponse, ArticleCommentSimple, ArticleCommentRequest>  implements ArticleCommentApi {

    @Resource
    private ArticleCommentRepository articleCommentRepository;

    @Override
    public JpaRepositoryImplementation<ArticleComment, Serializable> getJpaRepository() {
        return articleCommentRepository;
    }

    @Override
    public Function<ArticleComment, ArticleCommentSimple> getConvertSimple() {
        return new ArticleCommentSimpleConvert();
    }

    @Override
    public Function<ArticleCommentRequest, ArticleComment> getConvertForm() {
        return new ArticleCommentEntityConvert();
    }

    @Override
    public Function<ArticleComment, ArticleCommentResponse> getConvertResponse() {
        return new ArticleCommentResponseConvert();
    }




}


