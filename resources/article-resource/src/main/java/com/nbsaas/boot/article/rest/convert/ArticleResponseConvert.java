package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.Article;
import com.nbsaas.boot.article.api.domain.response.ArticleResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;
/**
* 实体对象转化成响应对象
*/

public class ArticleResponseConvert  implements Converter<ArticleResponse,Article> {

    @Override
    public ArticleResponse convert(Article source) {
        ArticleResponse  result = new  ArticleResponse();
        BeanDataUtils.copyProperties(source, result);
                    if(source.getArticleCatalog()!=null){
                        result.setArticleCatalog(source.getArticleCatalog().getId());
                    }
                    if(source.getDocument()!=null){
                    result.setNote(source.getDocument().getNote());
                    }
                    if(source.getCreator()!=null){
                        result.setCreator(source.getCreator().getId());
                    }
                    if(source.getDocument()!=null){
                        result.setDocument(source.getDocument().getId());
                    }
                    if(source.getCreator()!=null){
                        result.setCreatorName(source.getCreator().getName());
                    }
                    if(source.getArticleCatalog()!=null){
                        result.setArticleCatalogName(source.getArticleCatalog().getName());
                    }
        return result;
    }

}

