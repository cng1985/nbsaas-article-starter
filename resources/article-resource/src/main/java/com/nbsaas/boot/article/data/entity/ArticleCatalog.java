package com.nbsaas.boot.article.data.entity;

import com.nbsaas.boot.code.annotation.CatalogClass;
import com.nbsaas.boot.code.annotation.FormAnnotation;
import com.nbsaas.boot.jpa.data.entity.CatalogEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * 文章分类
 * 
 * @author 年高
 *
 */
@CatalogClass
@FormAnnotation(title = "文章分类管理", model = "文章分类" )
@Data
@Entity
@Table(name = "bs_cms_article_catalog")
public class ArticleCatalog extends CatalogEntity {

	/**
	 * 父节点
	 */
	@ManyToOne
	private ArticleCatalog parent;

	/**
	 * 数量
	 */
	private Long amount;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
	private List<ArticleCatalog> children;

	@Override
	public Long getParentId() {
		if (parent != null) {
			return parent.getId();
		}
		return null;
	}

}
