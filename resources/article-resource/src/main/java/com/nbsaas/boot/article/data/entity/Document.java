package com.nbsaas.boot.article.data.entity;

import com.nbsaas.boot.code.annotation.*;
import com.nbsaas.boot.jpa.data.entity.UUIDEntity;
import com.nbsaas.boot.jpa.data.entity.User;
import lombok.Data;
import org.hibernate.annotations.Comment;

import javax.persistence.*;
import java.util.Date;


@ComposeView
@CreateByUser
@FormAnnotation(title = "文档管理", model = "文档管理")
@Data
@Entity
@Table(name = "bs_cms_document")
public class Document extends UUIDEntity {

    /**
     * 文章标题
     */
    @SearchItem(label = "标题",name = "title")
    @FormField(title = "标题", grid = true, col = 22,width = "200",required = true)
    private String title;


    /**
     * 封面
     */
    @FormField(title = "封面", grid = true, col = 22,required = true,type = InputType.image)
    private String logo;


    /**
     * 文章作者
     */
    @JoinColumn(name = "creator_id",foreignKey=@ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @Comment("文档作者")
    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private User creator;

    @NoSimple
    @Comment("文档内容")
    @Column(length = 5000)
    private String note;


    /**
     * 添加时间
     */
    @FormField(title = "添加时间", grid = true, col = 22,width = "111111",ignore = true)
    @Comment("添加时间")
    private Date addDate;

    /**
     * 最新修改时间
     */
    @Comment("最新修改时间")
    private Date lastDate;
}
