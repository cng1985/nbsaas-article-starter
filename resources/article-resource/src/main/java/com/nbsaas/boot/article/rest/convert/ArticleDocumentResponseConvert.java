package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleDocument;
import com.nbsaas.boot.article.api.domain.response.ArticleDocumentResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;
/**
* 实体对象转化成响应对象
*/

public class ArticleDocumentResponseConvert  implements Converter<ArticleDocumentResponse,ArticleDocument> {

    @Override
    public ArticleDocumentResponse convert(ArticleDocument source) {
        ArticleDocumentResponse  result = new  ArticleDocumentResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

