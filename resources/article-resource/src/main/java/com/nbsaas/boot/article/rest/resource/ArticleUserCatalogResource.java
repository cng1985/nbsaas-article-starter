package com.nbsaas.boot.article.rest.resource;

import com.nbsaas.boot.article.api.apis.ArticleUserCatalogApi;
import com.nbsaas.boot.article.data.entity.ArticleUserCatalog;
import com.nbsaas.boot.article.api.domain.request.ArticleUserCatalogRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleUserCatalogResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleUserCatalogSimple;
import com.nbsaas.boot.article.rest.convert.ArticleUserCatalogSimpleConvert;
import com.nbsaas.boot.article.rest.convert.ArticleUserCatalogEntityConvert;
import com.nbsaas.boot.article.rest.convert.ArticleUserCatalogResponseConvert;
import com.nbsaas.boot.article.data.repository.ArticleUserCatalogRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.function.Function;
/**
*   业务接口实现
*/
@Transactional
@Service
public class ArticleUserCatalogResource extends BaseResource<ArticleUserCatalog,ArticleUserCatalogResponse, ArticleUserCatalogSimple, ArticleUserCatalogRequest>  implements ArticleUserCatalogApi {

    @Resource
    private ArticleUserCatalogRepository articleUserCatalogRepository;

    @Override
    public JpaRepositoryImplementation<ArticleUserCatalog, Serializable> getJpaRepository() {
        return articleUserCatalogRepository;
    }

    @Override
    public Function<ArticleUserCatalog, ArticleUserCatalogSimple> getConvertSimple() {
        return new ArticleUserCatalogSimpleConvert();
    }

    @Override
    public Function<ArticleUserCatalogRequest, ArticleUserCatalog> getConvertForm() {
        return new ArticleUserCatalogEntityConvert();
    }

    @Override
    public Function<ArticleUserCatalog, ArticleUserCatalogResponse> getConvertResponse() {
        return new ArticleUserCatalogResponseConvert();
    }




}


