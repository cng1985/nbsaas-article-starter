package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.Document;
import com.nbsaas.boot.article.api.domain.request.DocumentRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;
            import com.nbsaas.boot.jpa.data.entity.User;

/**
* 请求对象转换成实体对象
*/

public class DocumentEntityConvert  implements Converter<Document, DocumentRequest> {

    @Override
    public Document convert(DocumentRequest source) {
        Document result = new Document();
        BeanDataUtils.copyProperties(source, result);
                    if(source.getCreator()!=null){
                    User creator =new User();
                    creator.setId(source.getCreator());
                    result.setCreator(creator);
                    }
        return result;
    }
}

