package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleComment;
import com.nbsaas.boot.article.api.domain.simple.ArticleCommentSimple;

import com.nbsaas.boot.rest.api.Converter;

/**
* 列表对象转换器
*/

public class ArticleCommentSimpleConvert implements Converter<ArticleCommentSimple, ArticleComment> {




@Override
public ArticleCommentSimple convert(ArticleComment source) {
    ArticleCommentSimple result = new ArticleCommentSimple();

                result.setNote(source.getNote());
                if(source.getCreator()!=null){
                    result.setCreator(source.getCreator().getId());
                }
                result.setId(source.getId());
                result.setTitle(source.getTitle());
                result.setAddDate(source.getAddDate());
                if(source.getArticle()!=null){
                    result.setArticle(source.getArticle().getId());
                }
                result.setLastDate(source.getLastDate());


    return result;
}

}