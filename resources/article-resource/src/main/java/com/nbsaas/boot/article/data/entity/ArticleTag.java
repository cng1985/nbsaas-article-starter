package com.nbsaas.boot.article.data.entity;

import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 文章标签
 * 
 * @author 年高
 *
 */
@Data
@Entity
@Table(name = "bs_cms_article_tag")
public class ArticleTag extends AbstractEntity {

	private String name;

	private Integer size;

}
