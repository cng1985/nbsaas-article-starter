package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleUserCatalog;
import com.nbsaas.boot.article.api.domain.simple.ArticleUserCatalogSimple;

import com.nbsaas.boot.rest.api.Converter;

/**
* 列表对象转换器
*/

public class ArticleUserCatalogSimpleConvert implements Converter<ArticleUserCatalogSimple, ArticleUserCatalog> {




@Override
public ArticleUserCatalogSimple convert(ArticleUserCatalog source) {
    ArticleUserCatalogSimple result = new ArticleUserCatalogSimple();

                result.setCode(source.getCode());
                result.setDepth(source.getDepth());
                result.setName(source.getName());
                result.setIds(source.getIds());
                result.setSortNum(source.getSortNum());
                result.setId(source.getId());
                result.setLft(source.getLft());
                result.setAddDate(source.getAddDate());
                result.setNums(source.getNums());
                result.setRgt(source.getRgt());
                result.setLastDate(source.getLastDate());


    return result;
}

}