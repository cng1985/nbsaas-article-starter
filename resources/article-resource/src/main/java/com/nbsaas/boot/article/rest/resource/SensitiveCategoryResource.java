package com.nbsaas.boot.article.rest.resource;

import com.nbsaas.boot.article.api.apis.SensitiveCategoryApi;
import com.nbsaas.boot.article.data.entity.SensitiveCategory;
import com.nbsaas.boot.article.api.domain.request.SensitiveCategoryRequest;
import com.nbsaas.boot.article.api.domain.response.SensitiveCategoryResponse;
import com.nbsaas.boot.article.api.domain.simple.SensitiveCategorySimple;
import com.nbsaas.boot.article.rest.convert.SensitiveCategorySimpleConvert;
import com.nbsaas.boot.article.rest.convert.SensitiveCategoryEntityConvert;
import com.nbsaas.boot.article.rest.convert.SensitiveCategoryResponseConvert;
import com.nbsaas.boot.article.data.repository.SensitiveCategoryRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.function.Function;
/**
*   业务接口实现
*/
@Transactional
@Service
public class SensitiveCategoryResource extends BaseResource<SensitiveCategory,SensitiveCategoryResponse, SensitiveCategorySimple, SensitiveCategoryRequest>  implements SensitiveCategoryApi {

    @Resource
    private SensitiveCategoryRepository sensitiveCategoryRepository;

    @Override
    public JpaRepositoryImplementation<SensitiveCategory, Serializable> getJpaRepository() {
        return sensitiveCategoryRepository;
    }

    @Override
    public Function<SensitiveCategory, SensitiveCategorySimple> getConvertSimple() {
        return new SensitiveCategorySimpleConvert();
    }

    @Override
    public Function<SensitiveCategoryRequest, SensitiveCategory> getConvertForm() {
        return new SensitiveCategoryEntityConvert();
    }

    @Override
    public Function<SensitiveCategory, SensitiveCategoryResponse> getConvertResponse() {
        return new SensitiveCategoryResponseConvert();
    }




}


