package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.Article;
import com.nbsaas.boot.article.api.domain.request.ArticleRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;
            import com.nbsaas.boot.article.data.entity.ArticleCatalog;
            import com.nbsaas.boot.jpa.data.entity.User;
            import com.nbsaas.boot.article.data.entity.ArticleDocument;

/**
* 请求对象转换成实体对象
*/

public class ArticleEntityConvert  implements Converter<Article, ArticleRequest> {

    @Override
    public Article convert(ArticleRequest source) {
        Article result = new Article();
        BeanDataUtils.copyProperties(source, result);
                    if(source.getArticleCatalog()!=null){
                    ArticleCatalog articleCatalog =new ArticleCatalog();
                    articleCatalog.setId(source.getArticleCatalog());
                    result.setArticleCatalog(articleCatalog);
                    }
                    if(source.getCreator()!=null){
                    User creator =new User();
                    creator.setId(source.getCreator());
                    result.setCreator(creator);
                    }
                    if(source.getDocument()!=null){
                    ArticleDocument document =new ArticleDocument();
                    document.setId(source.getDocument());
                    result.setDocument(document);
                    }
        return result;
    }
}

