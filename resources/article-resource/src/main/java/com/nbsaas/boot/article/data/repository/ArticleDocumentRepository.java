package com.nbsaas.boot.article.data.repository;

import com.nbsaas.boot.article.data.entity.ArticleDocument;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface ArticleDocumentRepository  extends  JpaRepositoryImplementation<ArticleDocument, Serializable>{

}