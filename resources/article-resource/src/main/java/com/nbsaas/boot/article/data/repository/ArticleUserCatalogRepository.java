package com.nbsaas.boot.article.data.repository;

import com.nbsaas.boot.article.data.entity.ArticleUserCatalog;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import java.io.Serializable;

public interface ArticleUserCatalogRepository  extends  JpaRepositoryImplementation<ArticleUserCatalog, Serializable>{

}