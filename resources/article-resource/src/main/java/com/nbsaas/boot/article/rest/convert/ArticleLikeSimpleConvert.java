package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleLike;
import com.nbsaas.boot.article.api.domain.simple.ArticleLikeSimple;

import com.nbsaas.boot.rest.api.Converter;

/**
* 列表对象转换器
*/

public class ArticleLikeSimpleConvert implements Converter<ArticleLikeSimple, ArticleLike> {




@Override
public ArticleLikeSimple convert(ArticleLike source) {
    ArticleLikeSimple result = new ArticleLikeSimple();

                result.setId(source.getId());
                result.setAddDate(source.getAddDate());
                result.setLastDate(source.getLastDate());


    return result;
}

}