package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.SensitiveCategory;
import com.nbsaas.boot.article.api.domain.simple.SensitiveCategorySimple;

import com.nbsaas.boot.rest.api.Converter;

/**
* 列表对象转换器
*/

public class SensitiveCategorySimpleConvert implements Converter<SensitiveCategorySimple, SensitiveCategory> {




@Override
public SensitiveCategorySimple convert(SensitiveCategory source) {
    SensitiveCategorySimple result = new SensitiveCategorySimple();

                result.setCode(source.getCode());
                result.setDepth(source.getDepth());
                result.setName(source.getName());
                result.setIds(source.getIds());
                result.setSortNum(source.getSortNum());
                result.setId(source.getId());
                result.setLft(source.getLft());
                result.setAddDate(source.getAddDate());
                result.setRgt(source.getRgt());
                result.setLastDate(source.getLastDate());


    return result;
}

}