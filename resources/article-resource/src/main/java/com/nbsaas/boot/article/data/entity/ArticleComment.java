package com.nbsaas.boot.article.data.entity;

import com.nbsaas.boot.code.annotation.FieldConvert;
import com.nbsaas.boot.jpa.data.entity.AbstractEntity;
import com.nbsaas.boot.jpa.data.entity.User;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 文章评论
 * 
 * @author 年高
 *
 */
@Data
@Entity
@Table(name = "bs_cms_article_comment")
public class ArticleComment extends AbstractEntity {



	private String note;

	private String title;

	@FieldConvert
	@ManyToOne(fetch = FetchType.LAZY)
	private Article article;

	@FieldConvert
	@ManyToOne(fetch = FetchType.LAZY)
	private User creator;

}
