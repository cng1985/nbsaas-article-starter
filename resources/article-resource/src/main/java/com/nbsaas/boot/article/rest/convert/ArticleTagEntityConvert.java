package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleTag;
import com.nbsaas.boot.article.api.domain.request.ArticleTagRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class ArticleTagEntityConvert  implements Converter<ArticleTag, ArticleTagRequest> {

    @Override
    public ArticleTag convert(ArticleTagRequest source) {
        ArticleTag result = new ArticleTag();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

