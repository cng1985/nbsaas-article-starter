package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleCatalog;
import com.nbsaas.boot.article.api.domain.request.ArticleCatalogRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class ArticleCatalogEntityConvert  implements Converter<ArticleCatalog, ArticleCatalogRequest> {

    @Override
    public ArticleCatalog convert(ArticleCatalogRequest source) {
        ArticleCatalog result = new ArticleCatalog();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

