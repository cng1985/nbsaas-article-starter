package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.SensitiveCategory;
import com.nbsaas.boot.article.api.domain.response.SensitiveCategoryResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;
/**
* 实体对象转化成响应对象
*/

public class SensitiveCategoryResponseConvert  implements Converter<SensitiveCategoryResponse,SensitiveCategory> {

    @Override
    public SensitiveCategoryResponse convert(SensitiveCategory source) {
        SensitiveCategoryResponse  result = new  SensitiveCategoryResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

