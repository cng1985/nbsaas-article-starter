package com.nbsaas.boot.article.data.entity;

import com.nbsaas.boot.jpa.data.entity.CatalogEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * 文章分类
 * 
 * @author 年高
 *
 */
@Data
@Entity
@Table(name = "bs_cms_article_user_catalog")
public class ArticleUserCatalog extends CatalogEntity {

	/**
	 * 父节点
	 */
	@ManyToOne
	private ArticleUserCatalog parent;

	/**
	 * 数量
	 */
	private Long nums;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
	private List<ArticleUserCatalog> children;

	@Override
	public Long getParentId() {
		if (parent != null) {
			return parent.getId();
		}
		return null;
	}

}
