package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.Document;
import com.nbsaas.boot.article.api.domain.simple.DocumentSimple;

import com.nbsaas.boot.rest.api.Converter;

/**
* 列表对象转换器
*/

public class DocumentSimpleConvert implements Converter<DocumentSimple, Document> {




@Override
public DocumentSimple convert(Document source) {
    DocumentSimple result = new DocumentSimple();

                if(source.getCreator()!=null){
                    result.setCreator(source.getCreator().getId());
                }
                if(source.getCreator()!=null){
                    result.setCreatorName(source.getCreator().getName());
                }
                result.setLogo(source.getLogo());
                result.setId(source.getId());
                result.setTitle(source.getTitle());
                result.setAddDate(source.getAddDate());
                result.setLastDate(source.getLastDate());


    return result;
}

}