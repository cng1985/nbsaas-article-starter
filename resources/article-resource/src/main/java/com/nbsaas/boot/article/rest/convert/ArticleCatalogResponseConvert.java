package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.ArticleCatalog;
import com.nbsaas.boot.article.api.domain.response.ArticleCatalogResponse;

import com.nbsaas.boot.utils.BeanDataUtils;
import com.nbsaas.boot.rest.api.Converter;
/**
* 实体对象转化成响应对象
*/

public class ArticleCatalogResponseConvert  implements Converter<ArticleCatalogResponse,ArticleCatalog> {

    @Override
    public ArticleCatalogResponse convert(ArticleCatalog source) {
        ArticleCatalogResponse  result = new  ArticleCatalogResponse();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }

}

