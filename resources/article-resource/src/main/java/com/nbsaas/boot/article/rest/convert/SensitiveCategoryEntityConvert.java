package com.nbsaas.boot.article.rest.convert;

import com.nbsaas.boot.article.data.entity.SensitiveCategory;
import com.nbsaas.boot.article.api.domain.request.SensitiveCategoryRequest;

import org.springframework.beans.BeanUtils;
import com.nbsaas.boot.rest.api.Converter;
import com.nbsaas.boot.utils.BeanDataUtils;

/**
* 请求对象转换成实体对象
*/

public class SensitiveCategoryEntityConvert  implements Converter<SensitiveCategory, SensitiveCategoryRequest> {

    @Override
    public SensitiveCategory convert(SensitiveCategoryRequest source) {
        SensitiveCategory result = new SensitiveCategory();
        BeanDataUtils.copyProperties(source, result);
        return result;
    }
}

