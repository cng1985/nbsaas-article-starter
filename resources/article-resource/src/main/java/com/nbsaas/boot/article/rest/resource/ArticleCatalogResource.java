package com.nbsaas.boot.article.rest.resource;

import com.nbsaas.boot.article.api.apis.ArticleCatalogApi;
import com.nbsaas.boot.article.data.entity.ArticleCatalog;
import com.nbsaas.boot.article.api.domain.request.ArticleCatalogRequest;
import com.nbsaas.boot.article.api.domain.response.ArticleCatalogResponse;
import com.nbsaas.boot.article.api.domain.simple.ArticleCatalogSimple;
import com.nbsaas.boot.article.rest.convert.ArticleCatalogSimpleConvert;
import com.nbsaas.boot.article.rest.convert.ArticleCatalogEntityConvert;
import com.nbsaas.boot.article.rest.convert.ArticleCatalogResponseConvert;
import com.nbsaas.boot.article.data.repository.ArticleCatalogRepository;

import java.io.Serializable;
import com.nbsaas.boot.jpa.data.core.BaseResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.function.Function;
import com.nbsaas.boot.rest.request.PageRequest;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.boot.article.api.domain.request.ArticleCatalogSearch;
/**
*   业务接口实现
*/
@Transactional
@Service
public class ArticleCatalogResource extends BaseResource<ArticleCatalog,ArticleCatalogResponse, ArticleCatalogSimple, ArticleCatalogRequest>  implements ArticleCatalogApi {

    @Resource
    private ArticleCatalogRepository articleCatalogRepository;

    @Override
    public JpaRepositoryImplementation<ArticleCatalog, Serializable> getJpaRepository() {
        return articleCatalogRepository;
    }

    @Override
    public Function<ArticleCatalog, ArticleCatalogSimple> getConvertSimple() {
        return new ArticleCatalogSimpleConvert();
    }

    @Override
    public Function<ArticleCatalogRequest, ArticleCatalog> getConvertForm() {
        return new ArticleCatalogEntityConvert();
    }

    @Override
    public Function<ArticleCatalog, ArticleCatalogResponse> getConvertResponse() {
        return new ArticleCatalogResponseConvert();
    }



    @Override
    public ListResponse<ArticleCatalogSimple> list(PageRequest request) {
        ArticleCatalogSimpleConvert convert=new ArticleCatalogSimpleConvert();
        if (request instanceof ArticleCatalogSearch){
             ArticleCatalogSearch searchRequest=(ArticleCatalogSearch)request;
             convert.setFetch(searchRequest.getFetch());
        }
        return listSimple(request,convert);
    }

}


